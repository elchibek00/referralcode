<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Network;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use const http\Client\Curl\AUTH_ANY;

class UserController extends Controller
{
    public function loadRegister()
    {
        return view('register');
    }

    public function registered(UserRequest $request)
    {
        $validatedUser = $request->validated();
        $validatedPassword = Hash::make($validatedUser['password']);
        $referralCode = Str::random(10);
        $token = Str::random(50);

        if (isset($validatedUser['referral_code'])){
            $userData = User::where('referral_code', $validatedUser['referral_code'])->get();

            if (count($userData) > 0){
                $user = User::create([
                    'name' => $validatedUser['name'],
                    'email' => $validatedUser['email'],
                    'password' => $validatedPassword,
                    'referral_code' => $referralCode,
                    'remember_token' => $token
                ]);

                Network::create([
                    'referral_code' => $validatedUser['referral_code'],
                    'user_id' => $user->id,
                    'parent_user_id' => $userData[0]['id']
                ]);
            }else{
                return back()->with('error', 'Please enter a valid Referral Code');
            }

        }else{
            User::create([
                'name' => $validatedUser['name'],
                'email' => $validatedUser['email'],
                'password' => $validatedPassword,
                'referral_code' => $referralCode,
                'remember_token' => $token
            ]);
        }

        $domain = URL::to('/');
        $url = $domain . '/referral-register?ref=' . $referralCode;

        $data['url'] = $url;
        $data['name'] = $validatedUser['name'];
        $data['email'] = $validatedUser['email'];
        $data['password'] = $validatedUser['password'];
        $data['title'] = 'Registered';

        Mail::send('emails.registerMail', ['data' => $data], function ($message) use ($data) {
            $message->to($data['email'])->subject($data['title']);
        });

        // verification

        $url = $domain . '/email-verification/' . $token;

        $data['url'] = $url;
        $data['name'] = $validatedUser['name'];
        $data['email'] = $validatedUser['email'];
        $data['password'] = $validatedUser['password'];
        $data['title'] = 'Referral Verification Mail';

        Mail::send('emails.verifyMail', ['data' => $data], function ($message) use ($data) {
            $message->to($data['email'])->subject($data['title']);
        });

        return back()->with('success', 'Your registration has been successful & please verify your Mail');
    }

    public function loadRegisterReferral(Request $request)
    {

        if (isset($request->ref)){
            $referral = $request->ref;
            $userData = User::where('referral_code', $referral)->get();

            if(count($userData) > 0)
            {
                return view('referralRegister', compact('referral'));
            }else{
                return view('404');
            }
        }else{
           return redirect('/');
        }
    }

    public function emailVerification($token)
    {
        $userData = User::where('remember_token', $token)->get();

        if (count($userData) > 0){

            if ($userData[0]['is_verified'] == 1) {
                return view('verified', ['message' => 'Your email already verified!']);
            }

            User::where('id', $userData[0]['id'])->update([
                'is_verified' => 1,
                'email_verified_at' => date('Y-m-d H:i:s')
            ]);
            return view('verified', ['message' => 'Your' . $userData[0]['email'] . 'mail successfully verified!']);

        }else{
            return view('verified', ['message' => '404 Page not found']);
        }
    }

    public function loadLogin()
    {
        return view('login');
    }

    public function userLogin(Request $request)
    {
        $request->validate([
            'email' => ['bail', 'required', 'string', 'email'],
            'password' => ['bail', 'required']
        ]);

        $userData = User::where('email', $request['email'])->first();
        if (!empty($userData)){
            if ($userData->is_verified == 0){
                return back()->with('error', 'Please verify your mail');
            }
        }
        $userCredential = $request->only('email', 'password');
        if (Auth::attempt($userCredential)){
            return redirect('/dashboard');
        }else{
            return back()->with('error', 'Username & Password incorrect!');
        }

    }

    public function loadDashboard()
    {
        $networkCounter = 10 * Network::where('parent_user_id', Auth::id())->orWhere('user_id', Auth::id())->count();
        $networkData = Network::with('user')->where('parent_user_id', Auth::id())->get();

        $shareComponent = \Share::page(
            URL::to('/') . 'referral-register?ref=' . Auth::user()->referral_code,
            'Share and Earn Points by Referral Link',
        )
            ->facebook()
            ->twitter()
            ->linkedin()
            ->telegram()
            ->whatsapp()
        ;

        return view('dashboard', compact('networkCounter', 'networkData', 'shareComponent'));
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return redirect('/');
    }

    public function referralTrack()
    {
        $dateLabels = [];
        $dateData = [];

        for ($i = 30; $i >=0; $i--) {
            $dateLabels[] = Carbon::now()->subDays($i)->format('d-m-Y');
            $dateData[] = Network::whereDate('created_at', Carbon::now()->subDays($i)->format('Y-m-d'))
            ->where('parent_user_id', Auth::id())->count();
        }

        $dateLabels = json_encode($dateLabels);
        $dateData = json_encode($dateData);

        return view('referralTrack', compact(['dateLabels', 'dateData']));
    }

    public function deleteAccount(Request $request)
    {
        try {
            User::where('id', Auth::id())->delete();
            $request->session()->flush();
            Auth::logout();

            return response()->json(['success' => true]);
        }catch (\Exception $e){
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }
}
