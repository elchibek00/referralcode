<h1>Register</h1>
<form action="{{route('registered')}}" method="POST">
    @csrf
    <input type="text" name="name" placeholder="Enter Name">
    @error('name')
        <span style="color: red">{{$message}}</span>
    @enderror
    <br><br>
    <input type="email" name="email" placeholder="Enter Email">
    @error('email')
    <span style="color: red">{{$message}}</span>
    @enderror
    <br><br>
    <input type="text" name="referral_code" placeholder="Enter Referral Code (optional)">
    @error('referral_code')
    <span style="color: red">{{$message}}</span>
    @enderror
    <br><br>
    <input type="password" name="password" placeholder="Enter Password">
    @error('password')
    <span style="color: red">{{$message}}</span>
    @enderror
    <br><br>
    <input type="password" name="password_confirmation" placeholder="Confirm Password">
    <br><br>
    <input type="submit" value="Register">
</form>

@if(\Illuminate\Support\Facades\Session::has('success'))
    <p style="color: green">{{\Illuminate\Support\Facades\Session::get('success')}}</p>
@endif

@if(\Illuminate\Support\Facades\Session::has('error'))
    <p style="color: red">{{\Illuminate\Support\Facades\Session::get('error')}}</p>
@endif
