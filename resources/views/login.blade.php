<h1>Login</h1>
<form action="{{route('login')}}" method="POST">
    @csrf
    <input type="email" name="email" placeholder="Enter Email">
    @error('email')
        <span style="color: red;">{{$message}}</span>
    @enderror <br><br>
    <input type="password" name="password" placeholder="Enter Password">
    @error('password')
    <span style="color: red;">{{$message}}</span>
    @enderror <br><br>
    <input type="submit" value="Login">
</form>
@if(\Illuminate\Support\Facades\Session::has('error'))
    <p style="color: red">{{\Illuminate\Support\Facades\Session::get('error')}}</p>
@endif
